# Cookbook Name:: omnibus-gitlab
# Recipe:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'
require 'chef-vault'

describe 'omnibus-gitlab::default' do
  context 'with basic gitlab.rb' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
        node.normal['omnibus-gitlab']['gitlab_rb']['external_url'] = 'http://herpderp-extern.com'
        node.normal['omnibus-gitlab']['gitlab_rb']['pages_external_url'] = 'http://herpderp-pages.com'
        node.normal['omnibus-gitlab']['gitlab_rb']['logging']['udp_log_shipping_host'] = '127.0.0.1'
        node.normal['omnibus-gitlab']['gitlab_rb']['gitlab_exporter']['listen_address'] = '0.0.0.0'
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.not_to raise_error
    end

    it 'creates gitlab.rb' do
      expect(chef_run).to create_template('/etc/gitlab/gitlab.rb').with(
        mode: '0600'
      )
    end

    it 'sets the logging name' do
      expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
        expect(c).to include("external_url 'http://herpderp-extern.com'")
        expect(c).to include("pages_external_url 'http://herpderp-pages.com'")
        expect(c).to include("logging['udp_log_shipping_hostname'] = 'fauxhai.local'")
        expect(c).to include("gitlab_exporter['listen_address'] = \"0.0.0.0\"")
      }
    end

    it 'executes reconfigure' do
      expect(chef_run.template('/etc/gitlab/gitlab.rb')).to notify('execute[gitlab-ctl reconfigure]').to(:run)
      expect(chef_run.execute('gitlab-ctl reconfigure')).to do_nothing
      expect(chef_run.execute('gitlab-ctl reconfigure')).to have_attributes(
        environment: { 'CONFIG' => '' }
      )
    end

    context 'when `skip_auto_reconfigure` is set' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
          node.normal['omnibus-gitlab']['skip_auto_reconfigure'] = true
        end.converge(described_recipe)
      end

      it 'creates the skip file' do
        expect(chef_run).to create_file('/etc/gitlab/skip-auto-reconfigure')
      end
    end

    context 'when CloudFlare Origin Pull is enforced' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
          node.normal['omnibus-gitlab']['cloudflare']['origin_pull']['enforced'] = true
        end.converge(described_recipe)
      end

      it 'downloads the certificate' do
        expect(chef_run).to create_remote_file('/etc/gitlab/ssl/cf-origin-pull.pem').with(mode: '0600')
      end

      it 'notifies nginx to reload' do
        expect(chef_run.remote_file('/etc/gitlab/ssl/cf-origin-pull.pem')).to notify('bash[reload nginx configuration]').to(:run)
      end

      it 'sets nginx config' do
        expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
          expect(c).to include("nginx['ssl_verify_client'] = \"on\"")
          expect(c).to include("nginx['ssl_client_certificate'] = \"/etc/gitlab/ssl/cf-origin-pull.pem\"")
        }
      end
    end

    context 'when package install is enabled' do
      before do
        allow(Dir).to receive(:exist?).with('/somedir').and_return(true)
      end

      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
          node.normal['omnibus-gitlab']['package']['name'] = "gitlab-ee"
          node.normal['omnibus-gitlab']['package']['version'] = "11.9.8-ee.0"
          node.normal['omnibus-gitlab']['package']['prom_metric_file'] = '/somedir/omnibus_package.prom'
        end.converge(described_recipe)
      end

      it 'installs the gitlab-ee package with name and version' do
        expect(chef_run).to install_apt_package('gitlab-ee').with(
          version: "11.9.8-ee.0"
        )
      end

      it 'creates the metric file' do
        expect(chef_run).to create_file('/somedir/omnibus_package.prom').with(
          mode: '0644',
          content: "omnibus_package_install{enable=\"true\"} 1.0\n"
        )
      end
    end

    context 'when package install is not enabled' do
      before do
        allow(Dir).to receive(:exist?).with('/somedir').and_return(true)
      end

      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
          node.normal['omnibus-gitlab']['package']['name'] = "gitlab-ee"
          node.normal['omnibus-gitlab']['package']['version'] = "11.9.8-ee.0"
          node.normal['omnibus-gitlab']['package']['enable'] = false
          node.normal['omnibus-gitlab']['package']['prom_metric_file'] = '/somedir/omnibus_package.prom'
        end.converge(described_recipe)
      end

      it 'does not install the gitlab-ee package with name and version' do
        expect(chef_run).not_to install_apt_package('gitlab-ee').with(
          version: "11.9.8-ee.0"
        )
      end

      it 'creates the metric file' do
        expect(chef_run).to create_file('/somedir/omnibus_package.prom').with(
          mode: '0644',
          content: "omnibus_package_install{enable=\"false\"} 1.0\n"
        )
      end
    end

    context 'when `skip_auto_reconfigure` is not set' do
      it 'deletes the skip file' do
        expect(chef_run).to delete_file('/etc/gitlab/skip-auto-reconfigure')
      end
    end

    context 'with SSL configuration' do
      let(:certificate) { 'fake-cert' }
      let(:private_key) { 'fake-key' }

      context 'when Gitaly SSL configuration is set' do
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['gitaly_certificate'] = certificate
            node.normal['omnibus-gitlab']['ssl']['gitaly_private_key'] = private_key
          end.converge(described_recipe)
        end

        it 'creates the certificate and private key files' do
          expect(chef_run).to create_file('/etc/gitlab/ssl/gitaly.crt').with(
            group: 'git',
            content: certificate
          )
          expect(chef_run).to create_file('/etc/gitlab/ssl/gitaly.key').with(
            group: 'git',
            mode: '0640',
            content: private_key
          )
        end
      end

      context 'when Praefect SSL configuration is set' do
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['praefect_certificate'] = certificate
            node.normal['omnibus-gitlab']['ssl']['praefect_private_key'] = private_key
          end.converge(described_recipe)
        end

        it 'creates the certificate and private key files' do
          expect(chef_run).to create_file('/etc/gitlab/ssl/praefect.crt').with(
            group: 'git',
            content: certificate
          )
          expect(chef_run).to create_file('/etc/gitlab/ssl/praefect.key').with(
            group: 'git',
            mode: '0640',
            content: private_key
          )
        end
      end

      context 'when nginx SSL configuration is set' do
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['certificate'] = certificate
            node.normal['omnibus-gitlab']['ssl']['private_key'] = private_key
          end.converge(described_recipe)
        end

        it 'creates the certificate and private key files' do
          expect(chef_run).to create_file('/etc/gitlab/ssl/nginx.crt').with(
            content: certificate
          )
          expect(chef_run).to create_file('/etc/gitlab/ssl/nginx.key').with(
            mode: '0600',
            content: private_key
          )
        end
      end

      context 'when mattermost SSL configuration is set' do
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['mattermost_certificate'] = certificate
            node.normal['omnibus-gitlab']['ssl']['mattermost_private_key'] = private_key
          end.converge(described_recipe)
        end

        it 'creates the certificate and private key files' do
          expect(chef_run).to create_file('/etc/gitlab/ssl/mattermost-nginx.crt').with(
            content: certificate
          )
          expect(chef_run).to create_file('/etc/gitlab/ssl/mattermost-nginx.key').with(
            mode: '0600',
            content: private_key
          )
        end
      end

      context 'when pages SSL configuration is set' do
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['pages_certificate'] = certificate
            node.normal['omnibus-gitlab']['ssl']['pages_private_key'] = private_key
          end.converge(described_recipe)
        end

        it 'creates the certificate and private key files' do
          expect(chef_run).to create_file('/etc/gitlab/ssl/pages.crt').with(
            content: certificate
          )
          expect(chef_run).to create_file('/etc/gitlab/ssl/pages.key').with(
            mode: '0600',
            content: private_key
          )
        end
      end

      context 'when registry SSL configuration is set' do
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['registry_certificate'] = certificate
            node.normal['omnibus-gitlab']['ssl']['registry_private_key'] = private_key
          end.converge(described_recipe)
        end

        it 'creates the certificate and private key files' do
          expect(chef_run).to create_file('/etc/gitlab/ssl/registry.crt').with(
            content: certificate
          )
          expect(chef_run).to create_file('/etc/gitlab/ssl/registry.key').with(
            mode: '0600',
            content: private_key
          )
        end
      end

      context 'when trusted certificates are set' do
        let(:certificate_2) { 'another-cert' }
        let(:chef_run) do
          ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
            node.normal['omnibus-gitlab']['ssl']['trusted_certs'] = {
              'trusted.crt' => certificate,
              'another.crt' => certificate_2
            }
          end.converge(described_recipe)
        end

        it 'creates the certificate files' do
          expect(chef_run).to create_file('/etc/gitlab/trusted-certs/trusted.crt').with(
            content: certificate
          )
          expect(chef_run).to create_file('/etc/gitlab/trusted-certs/another.crt').with(
            content: certificate_2
          )
        end
      end
    end
  end
end
