#
# Cookbook:: cookbook-omnibus-gitlab
# Recipe:: default
# License:: MIT
#
# # Copyright:: 2019, GitLab Inc.

cf_origin_pull_enforced = node['omnibus-gitlab']['cloudflare']['origin_pull']['enforced']
cf_origin_pull_cert_path = '/etc/gitlab/ssl/cf-origin-pull.pem'

if cf_origin_pull_enforced
  node.default['omnibus-gitlab']['gitlab_rb']['nginx']['ssl_verify_client'] = 'on'
  node.default['omnibus-gitlab']['gitlab_rb']['nginx']['ssl_client_certificate'] = cf_origin_pull_cert_path
end

#
# merge_secrets takes the passed string (or array of strings),
#   gets the secret configuration at that location,
#   and merges the secrets with the node attributes
#   returning a hash of normal and secret attributes.
#

attributes_with_secrets = merge_secrets('omnibus-gitlab')

omnibus_pkg = node['omnibus-gitlab']['package']
pkg_url =
  if omnibus_pkg['use_key']
    "#{omnibus_pkg['scheme_url']}://#{attributes_with_secrets['package']['key']}:@#{omnibus_pkg['base_url']}"
  else
    "#{omnibus_pkg['scheme_url']}://#{omnibus_pkg['base_url']}"
  end
pkg_repo = omnibus_pkg['repo']

package 'curl'

case node['platform_family']
when 'debian'
  include_recipe 'apt::default'

  execute "add #{pkg_url}/#{pkg_repo} apt repo" do
    command "curl #{pkg_url}/install/repositories/#{pkg_repo}/script.deb.sh | bash"
    creates "/etc/apt/sources.list.d/#{pkg_repo.sub('/', '_')}.list"
  end
  apt_package omnibus_pkg['name'] do
    version omnibus_pkg['version']
    options '--force-yes'
    timeout omnibus_pkg['timeout']
    notifies :run, 'execute[apt-get update]', :before
    notifies :run, 'execute[gitlab-ctl reconfigure]'
    only_if { omnibus_pkg['enable'] }
  end
when 'rhel'
  execute "add #{pkg_url}/#{pkg_repo} yum repo" do
    command "curl #{pkg_url}/install/repositories/#{pkg_repo}/script.rpm.sh | bash"
    creates "/etc/yum.repos.d/#{pkg_repo.sub('/', '_')}.repo"
  end

  yum_package omnibus_pkg['name'] do
    version omnibus_pkg['version']
    timeout omnibus_pkg['timeout']
    notifies :run, 'execute[gitlab-ctl reconfigure]'
    allow_downgrade true
    only_if { omnibus_pkg['enable'] }
  end
end

# Create /etc/gitlab and its contents
directory '/etc/gitlab'

# Fetch encrypted secrets and node attributes
gitlab_rb = attributes_with_secrets['gitlab_rb']

template '/etc/gitlab/gitlab.rb' do
  mode '0600'
  variables(gitlab_rb: gitlab_rb)
  helper(:single_quote) { |value| value.nil? ? nil : "'#{value}'" }
  notifies :run, 'execute[gitlab-ctl reconfigure]'
end

file '/etc/gitlab/skip-auto-reconfigure' do
  if node['omnibus-gitlab']['skip_auto_reconfigure']
    action :create
  else
    action :delete
  end
end

directory '/etc/gitlab/ssl' do
  owner 'root'
  group 'git'
  mode '0750'
end

# Fetch encrypted secrets and node attributes
ssl = attributes_with_secrets['ssl']

file node['omnibus-gitlab']['gitlab_rb']['nginx']['ssl_certificate'] do
  content ssl['certificate']
  not_if { ssl['certificate'].empty? }
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['nginx']['ssl_certificate_key'] do
  content ssl['private_key']
  not_if { ssl['private_key'].empty? }
  mode '0600'
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['mattermost-nginx']['ssl_certificate'] do
  content ssl['mattermost_certificate']
  not_if { ssl['mattermost_certificate'].empty? }
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['mattermost-nginx']['ssl_certificate_key'] do
  content ssl['mattermost_private_key']
  not_if { ssl['mattermost_private_key'].empty? }
  mode '0600'
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['pages-nginx']['ssl_certificate'] do
  content ssl['pages_certificate']
  not_if { ssl['pages_certificate'].empty? }
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['pages-nginx']['ssl_certificate_key'] do
  content ssl['pages_private_key']
  not_if { ssl['pages_private_key'].empty? }
  mode '0600'
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['registry-nginx']['ssl_certificate'] do
  content ssl['registry_certificate']
  not_if { ssl['registry_certificate'].empty? }
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['registry-nginx']['ssl_certificate_key'] do
  content ssl['registry_private_key']
  not_if { ssl['registry_private_key'].empty? }
  mode '0600'
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['gitaly']['certificate_path'] do
  content ssl['gitaly_certificate']
  not_if { ssl['gitaly_certificate'].empty? }
  group 'git'
  notifies :run, 'execute[gitlab-ctl reconfigure]'
end

file node['omnibus-gitlab']['gitlab_rb']['gitaly']['key_path'] do
  content ssl['gitaly_private_key']
  not_if { ssl['gitaly_private_key'].empty? }
  group 'git'
  mode '0640'
  notifies :run, 'execute[gitlab-ctl reconfigure]'
end

file node['omnibus-gitlab']['gitlab_rb']['praefect']['certificate_path'] do
  content ssl['praefect_certificate']
  not_if { ssl['praefect_certificate'].empty? }
  group 'git'
  notifies :run, 'execute[gitlab-ctl reconfigure]'
end

file node['omnibus-gitlab']['gitlab_rb']['praefect']['key_path'] do
  content ssl['praefect_private_key']
  not_if { ssl['praefect_private_key'].empty? }
  group 'git'
  mode '0640'
  notifies :run, 'execute[gitlab-ctl reconfigure]'
end

# Create /etc/gitlab/trusted-certs and its contents
directory '/etc/gitlab/trusted-certs'

ssl['trusted_certs'].each do |file_name, file_content|
  file File.join('/', 'etc', 'gitlab', 'trusted-certs', file_name) do
    content file_content
    notifies :run, 'bash[reload nginx configuration]'
  end
end

remote_file cf_origin_pull_cert_path do
  source node['omnibus-gitlab']['cloudflare']['origin_pull']['certificate_source']
  mode '0600'
  atomic_update true
  retries 3
  action :create
  notifies :run, 'bash[reload nginx configuration]'
  use_conditional_get true
  use_etag true
  only_if { node['omnibus-gitlab']['cloudflare']['origin_pull']['enforced'] }
end

# Run gitlab-ctl reconfigure if /etc/gitlab/gitlab.rb changed
execute 'gitlab-ctl reconfigure' do
  action :nothing
  # Set CONFIG to an empty string since gitlab-pages will
  # uses it as a config override
  # https://gitlab.com/gitlab-com/gl-infra/delivery/issues/612
  environment 'CONFIG' => ''
  only_if { node['omnibus-gitlab']['run_reconfigure'] }
end

# Reload NGINX if the SSL certificate or key has changed
bash 'reload nginx configuration' do
  code <<-SHELL
  if gitlab-ctl status nginx ; then
    gitlab-ctl hup nginx
  fi
  SHELL
  action :nothing
end

# Drop a metric indicating whether package updates are enabled
file omnibus_pkg['prom_metric_file'] do
  content %(omnibus_package_install{enable="#{omnibus_pkg['enable']}"} 1.0\n)
  mode "0644"
  only_if { ::Dir.exist?(File.dirname(omnibus_pkg['prom_metric_file'])) }
end
